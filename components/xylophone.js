class Sound {

  constructor(context) {
    this.context = context;
  }

  setup() {
    this.oscillator = this.context.createOscillator();
    this.gainNode = this.context.createGain();

    this.oscillator.connect(this.gainNode);
    this.gainNode.connect(this.context.destination);
    this.oscillator.type = 'sine';
  }

  play(value) {
    this.setup();

    this.oscillator.frequency.value = value;
    this.gainNode.gain.setValueAtTime(1, this.context.currentTime);

    this.oscillator.start(this.context.currentTime);
    this.stop(this.context.currentTime);
  }

  stop() {
    this.gainNode.gain.exponentialRampToValueAtTime(0.001, this.context.currentTime + 1);
    this.oscillator.stop(this.context.currentTime + 1);
  }

}

let context = new (window.AudioContext || window.webkitAudioContext)();

AFRAME.registerComponent('xylophone', {
  schema: {
    position: {type: 'vec3', default: {x: -0.2, y: 1.2, z: -0.5}}
  },
  init: function () {
    var colors = [
      'crimson',
      'tomato',
      'goldenrod',
      'palegreen',
      'mediumseagreen',
      'lightseagreen',
      'royalblue',
      'mediumslateblue'
    ];

    var frequencies = [
      261.63,
      293.66,
      329.63,
      349.23,
      392.00,
      440.00,
      493.88,
      523.25
    ];

    var barGroup = document.createElement('a-entity');
    // A-Frame Components
    barGroup.setAttribute('position', this.data.position);

    for (var i = 0, iMax = 8; i < iMax; i += 1) {
      var bar = document.createElement('a-box');
      var positionX = i * 0.055;
      var depth = 0.2 - (i * 0.01);
      let sound = new Sound(context);
      let frequency = frequencies[i];

      // Super-hands Components
      bar.setAttribute('clickable');
      // A-Frame Components
      bar.setAttribute('color', colors[i]);
      bar.setAttribute('shadow', 'cast: true; receive: false');
      bar.setAttribute('depth', depth);
      bar.setAttribute('width', 0.05);
      bar.setAttribute('height', 0.01);
      bar.setAttribute('position', positionX + ' 0 0' );
      // Animation Components
      bar.setAttribute('animation__anim-hit', 'property: scale; startEvents: mousedown; from: 1 1 1; to: 0.95 0.95 0.95; dur: 100; easing: linear;');
      bar.setAttribute('animation__anim-bounceback', 'property: scale; startEvents: mouseup; from: 0.95 0.95 0.95; to: 1 1 1; dur: 100; easing: linear;');
      // Sound
      bar.addEventListener('mousedown', function() {
        sound.play(frequency);
        sound.stop();
      });

      barGroup.append(bar);
    }
    this.el.appendChild(barGroup);
  }
});

AFRAME.registerPrimitive('a-xylophone', {
  defaultComponents: {
    xylophone: {}
  },
  mappings: {
    position: 'xylophone.position'
  }
});
